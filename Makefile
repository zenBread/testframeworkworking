CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline \
-std=c11 -Wall -Wextra
SOLDIR=SolutionFiles
SOLOBJDIR=SolutionLibrary


.default: libSolutions.a
.phony: check

ARFLAGS = Urv
libSolutions.a: libSolutions.a($(SOLDIR)/Cllist.o $(SOLDIR)/Llist.o $(SOLDIR)/Dllist.o)
	mv libSolutions.a $(SOLOBJDIR)/
check: Tests/testAll
	./$^

clean:
	@$(RM) -r Tests/*.o Tests/testAll

TESTOBJS = $(subst .c,.o,$(wildcard Tests/test*.c))


Tests/testAll: $(TESTOBJS)
Tests/testAll: CFLAGS += -I. -g
Tests/testAll: LDFLAGS += -L./$(SOLOBJDIR)
Tests/testAll: LDLIBS += -lcunit -lSolutions