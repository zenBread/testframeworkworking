#include "../Headers/Graph.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Graph Methods to implement

Pgraph
graphNew(void)
{
    Pgraph retGraph = (Pgraph)calloc(1, sizeof(Pgraph));
    return retGraph;
}

bool
graphAddNode(Pgraph pGraph, int data)
{
    if (!pGraph) {
        return false;
    }

    // allocate Node
    Node *newNode = (Node *)calloc(1, sizeof(Node));
    if (!newNode) {
        return false;
    }

    // save int data to Node
    newNode->data = data;

    // is Pgraph->nodes == NULL?
    if (!pGraph->nodes) {
        pGraph->nodes = newNode;
        goto Done;
    }
    else
    {
        // walk the adjList (Pgraph->nodes) until end then add GraphNode
        Node *currNode = pGraph->nodes;

        while(currNode->next)
        {
            currNode = currNode->next;
        }

        // Found last node. Point to the new node.
        currNode->next = newNode;
    }

Done:
    return true;
}

bool
graphAddEdge(Pgraph pGraph, Node *srcNode, Node *dstNode)
{
    if (!pGraph || !srcNode || !dstNode) {
        return false;
    }

    Edge *newEdge = (Edge *)calloc(1, sizeof(Edge));
    if (!newEdge)
    {
        return false;
    }

    newEdge->weight = 1;
    newEdge->destNode = dstNode;

    // If the source Node has no previous edges, connect edge to src
    if (NULL == srcNode->edge)
    {
        srcNode->edge = newEdge;
    }
    else
    {
        // iterate to add at end of edges
        Edge *currEdge = srcNode->edge;
        while(currEdge->edgeNode)
        {
            currEdge = currEdge->edgeNode;
        }

        currEdge->edgeNode = newEdge;
    }

    return true;
}

Node *
graphFind(Pgraph pGraph, int data)
{
    if (!pGraph)
        return NULL;
    // Begin at first node
    Node *currNode = pGraph->nodes;
    Node *retVal = NULL;

    // Iterate Node list to find matching data
    while(currNode) {
        if (data == currNode->data) {
            retVal = currNode;
            break;
        }

        currNode = currNode->next;
    }

    return retVal;
}

// Removes all of a nodes edges.
static void _strip_node_edges(Node *node)
{
    if (NULL == node)
    {
        return;
    }

	Edge *edge = node->edge;
	while (edge) {
		Edge *tmpEdge = edge->edgeNode;
		free(edge);

		edge = tmpEdge;
	}

    node->edge = NULL;

    return;
}

bool
graphRmNode(Pgraph pGraph, int data)
{
	if (!pGraph) {
		return false;
	}

	bool removed = false;

    Node *currNode = pGraph->nodes;
	if (!currNode)
    {
		return removed;
	}

    // Part 1: Remove all edges first, by iterating through each node in the adjacency list
    while(currNode)
    {
        Edge *curEdge = currNode->edge;
        Edge *prevEdge = NULL;
        while(curEdge)
        {
            if (curEdge->destNode->data == data)
            {
                if (!prevEdge)
                {
                    currNode->edge = curEdge->edgeNode;
                    free(curEdge);
                    curEdge = currNode->edge;
                }
                else
                {
                    prevEdge->edgeNode = curEdge->edgeNode;
                    free(curEdge);
                    curEdge = prevEdge->edgeNode;
                }
            }
            else
            {
                prevEdge = curEdge;
                curEdge = curEdge->edgeNode;
            }
        }

        currNode = currNode->next;
    }

    // Part 2: Now that the edges have bene removed from other nodes edges, remove all nodes that match.
    currNode = pGraph->nodes;
	Node *prevNode = NULL;
    while(currNode)
    {
        // Check if current Node
		if (currNode->data == data)
        {
            if (NULL == prevNode)
            {
                pGraph->nodes= currNode->next;
                currNode = currNode->next;
                continue;
            }

			prevNode->next = currNode->next;

			_strip_node_edges(currNode);
			free(currNode);
			removed = true;
            currNode = prevNode->next;
		}
        else
        {
            prevNode = currNode;
            currNode = currNode->next;
        }
    }

	return removed;
}

bool
graphRmEdge(Pgraph pGraph, Node *srcNode, Node *dstNode)
{
    if (!pGraph || !srcNode || !dstNode) {
        return false;
    }
   
    // find edge src->dst
    Edge *curEdge = srcNode->edge;
    if (!curEdge) {
        return false;
    }

    if (curEdge->destNode->data == dstNode->data) {
        Edge *tmpEdge = curEdge;
        srcNode->edge = curEdge->edgeNode;
        free(tmpEdge);
        return true;
    }

    while(curEdge->edgeNode) {
        if (curEdge->edgeNode->destNode->data == dstNode->data) {
            Edge *tmpEdge = curEdge->edgeNode;
            curEdge->edgeNode = curEdge->edgeNode->edgeNode;
            free(tmpEdge);
            return true;
        }
        curEdge = curEdge->edgeNode;
    }

    return NULL;
}

bool
graphDestroy(Pgraph pGraph)
{
    if (!pGraph)
        return false;

    Node *currNode = pGraph->nodes;

    // Free all of the Nodes edges
    while(currNode) {
		_strip_node_edges(currNode);
        currNode = currNode->next;
    }

    // Free all of the nodes
    currNode = pGraph->nodes;
    Node *tmpNode = NULL;

    while(currNode) {
        tmpNode = currNode->next;
        free(currNode);

        currNode = tmpNode;
    }

    free(pGraph);
    return true;
}
