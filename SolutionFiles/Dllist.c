#include "../Headers/Dllist.h"

int dllistCompare(int a, int b)
{
    return a > b;
}

// Doubly Linked List Methods to implement

Dllist *dllistNew(void)
{
    Dllist *retDlist = (Dllist *)calloc(1, sizeof(Dllist));
    if (!retDlist)
        return NULL;

    return retDlist;
}

bool dllistAdd(Dllist *list, int data)
{
    if (!list)
        return false;

    DllistNode *node = calloc(1, sizeof(*node));
    if (!node) {
        return false;
    }
    node->data = data;

    if (!list->head){
        list->head = node;
        return true;
    }

    DllistNode *oldHead = list->head;
    list->head = node;
    node->next = oldHead;
    oldHead->prev = node;

    return true;
}

DllistNode *dllistFind(Dllist *list, int data)
{
    if (!list || !list->head)
        goto Done;

    DllistNode *curNode = list->head;

    while (curNode)
    {
        if (curNode->data == data)
            return curNode;
        else
            curNode = curNode->next;
    }

Done:
    return NULL;
}

bool dllistAddAfter(Dllist *list, int data, int after)
{
    if (!list || !list->head) {
        return false;
    }

    DllistNode *tmp = dllistFind(list, after);
    if (!tmp) {
        return false;
    }

    DllistNode *node = malloc(sizeof(*node));
    if (!node) {
        return false;
    }
    node->data = data;
    node->next = NULL;
    node->prev = NULL;

    node->next = tmp->next;
    node->prev = tmp;
    if (tmp->next) {
        tmp->next->prev = node;
    }
    tmp->next = node;

    return true;
}

bool dllistAddBefore(Dllist *list, int data, int before)
{
    if (!list || !list->head) {
        return false;
    }

    DllistNode *tmp = dllistFind(list, before);
    if (!tmp) {
        return false;
    }

    // Its the head of the list
    if (list->head == tmp) {
        return dllistAdd(list, data);
    }

    DllistNode *node = malloc(sizeof(*node));
    if (!node) {
        return false;
    }
    node->data = data;
    node->next = NULL;
    node->prev = NULL;

    tmp->prev->next = node;
    node->prev = tmp->prev;
    tmp->prev = node;
    node->next = tmp;

    return true;
}

bool dllistSort(Dllist *list, int (*Compare) (int, int))
{
    if (!list || !list->head) {
        return false;
    }
    DllistNode *node, *tmp;
    int data;
    for (node = list->head; node->next != NULL; node = node->next) {
        for (tmp = node->next; tmp != NULL; tmp = tmp->next) {
            if (Compare(node->data, tmp->data)) {
                data = node->data;
                node->data = tmp->data;
                tmp->data = data;
            }
        }
    }

    return true;
}

DllistNode *dllistRmHead(Dllist *list)
{
    if (!list || !list->head)
        return NULL;

    DllistNode *oldHead = list->head;
    DllistNode *newHead = NULL;

    if (oldHead->next) {
        newHead = oldHead->next;
        list->head = newHead;
        newHead->prev = NULL;
        oldHead->next = NULL;
    } else {
        list->head = NULL;
    }

    return oldHead;
}

DllistNode *dllistRm(Dllist *list, int data)
{
    if (!list || !list->head)
        return NULL;

    if (list->head->data == data) {
        return dllistRmHead(list);
    }

    DllistNode *curNode = list->head->next;
    DllistNode *tempNode = NULL;
    while (curNode) {
        if (curNode->data == data) {
            tempNode = curNode;
            curNode->prev->next = curNode->next;
            if (curNode->next) {
                curNode->next->prev = curNode->prev;
            }
            break;
            }

        curNode = curNode->next;
        }

    return tempNode;
}

bool dllistDestroy(Dllist *list)
{
    bool retVal = false;
    if (!list )
    {
        goto END;
    }
    else if (!list->head)
    {
        free(list);
        retVal = true;
        goto END;
    }

    DllistNode *tempNode = list->head;

    while(tempNode) {
        DllistNode *nextTemp = tempNode->next;
        free(tempNode);

        tempNode = nextTemp;
    }

    free(list);
    retVal = true;
END:
    return retVal;
}
