#include "../Headers/Tree.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Binary Tree Methods to implement

static TreeNode *recurseTreeFind(TreeNode *node, int data);
static void recurseTreeDestroy(TreeNode *node);

Ptree treeNew(void)
{
    Ptree retTree = (Ptree)calloc(1, sizeof(Ptree));
    if (retTree)
        retTree->head = NULL;

    return retTree;
}

bool treeAdd(Ptree tr, int data)
{
    bool retVal = false;
    if (!tr)
        return retVal;
    // if no head

    TreeNode *node = (TreeNode *)calloc(1, sizeof(TreeNode));
    if (!node)
        return retVal;
    node->data = data;

    if (!tr->head) {
        tr->head = node;
        goto Done;
    }

    TreeNode *curNode = tr->head;
    TreeNode *prevNode = NULL;

    /* got this form stack overflow
     * https://stackoverflow.com/questions/36795065/
     * binary-search-tree-insertion-c-without-recursion/36795782
     * by Llias Mentz 22APR2016 @ 15:04
     */
    while (curNode) {
        prevNode = curNode;
        if (node->data > curNode->data) {
            // go right
            curNode = curNode->right;
        } else {
            // go left
            curNode = curNode->left;
        }
    }
    if (prevNode->data < node->data) {
        prevNode->right = node;
    } else {
        prevNode->left = node;
    }

Done:
    return retVal = true;
}


TreeNode *treeFind(Ptree tr, int data)
{
    if (!tr)
        return NULL;

    return recurseTreeFind(tr->head, data);
}

static TreeNode *recurseTreeFind(TreeNode *node, int data)
{
    if (node->data == data) {
        return node;
    } else if (node->right && node->data < data) {
        return recurseTreeFind(node->right, data);
    } else if (node->left && node->data >= data) {
        return recurseTreeFind(node->left, data);
    }

    return NULL;
}

TreeNode *treeRmHead(Ptree tr)
{
    TreeNode *retVal = NULL;
    if (!tr)
        goto Done;

    // if head is empty
    if (!tr->head)
        goto Done;

    TreeNode *oldHead = tr->head;
    TreeNode *newHead = NULL;
    // cause right is bigger
    if (oldHead->right) {
        newHead = oldHead->right;
        // connect the left to the new head
        newHead->left = oldHead->left;
        // tree points to a new head
        tr->head = newHead;
    } else if (oldHead->left) {
        // is this needed?
        newHead = oldHead->left;
        // tree points to a new head
        tr->head = newHead;
    } else {
        // if oldHead left && right are NULL then NULL the tr->head
        tr->head = NULL;
    }

    // return oldHead;
    retVal = oldHead;

Done:
    return retVal;
}

bool treeDestroy(Ptree tr)
{
    bool retVal = false;
    if (!tr)
        return retVal;

    recurseTreeDestroy(tr->head);

    free(tr);


    return retVal = true;
}

static void recurseTreeDestroy(TreeNode *node)
{
    if (!node)
        return;

    recurseTreeDestroy(node->left);
    recurseTreeDestroy(node->right);
    free(node);
}
