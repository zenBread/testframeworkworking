
#include "../Headers/Llist.h"

int llistCompare(int a, int b)
{
    return a > b;
}


/* 
 * Singularly Linked List Methods to implement
 */

Llist *llistNew(void)
{
    Llist *retList = (Llist *)calloc(1, sizeof(Llist));
    if (!retList)
        return NULL;

    return retList;
}

bool llistAdd(Llist *list, int data)
{
    if (!list) {
        return false;
    }

    LlistNode *node = calloc(1, sizeof(LlistNode));
    if (!node) {
        return false;
    }
    node->data = data;
    node->next = NULL;

    if (!list->head) {
        list->head = node;
        return true;
    }

    LlistNode *oldHead = list->head;
    list->head = node;
    node->next = oldHead; 

    return true;
}

LlistNode *llistFind(Llist *list, int data)
{
    if (!list || !list->head)
        return NULL;

    LlistNode *curNode = list->head;

    while(curNode) {
        if (curNode->data == data) {
            return curNode;
        } else {
            curNode = curNode->next;
        }
    }

    return NULL;
}    

LlistNode *llistRmHead(Llist *list)
{
    if (!list || !list->head)
        return NULL;

    LlistNode *oldHead = list->head;
    LlistNode *newHead = NULL;

    if (oldHead->next) {
        newHead = oldHead->next;
    }

    list->head = newHead;

    return oldHead;
}    

LlistNode *llistRm(Llist *list, int data)
{
    if (!list || !list->head)
        return NULL;

    LlistNode *tempNode = NULL;
    //Check the head
    if (list->head->data == data) {
        return llistRmHead(list);
    }

    LlistNode *prevNode = list->head;
    LlistNode *curNode = prevNode->next;

    while(curNode) {
        if (curNode->data == data) {
            tempNode = curNode;
            prevNode->next = curNode->next;
            break;
        }

        prevNode = curNode;
        curNode = curNode->next;
    }
    return tempNode;
}

bool llistSort(Llist *l, int (*Compare) (int, int))
{
    if (!l || !l->head) {
        return false;
    }
    LlistNode *node, *tmp;
    int data;
    for (node = l->head; node->next != NULL; node = node->next) {
        for (tmp = node->next; tmp != NULL; tmp = tmp->next) {
            if (Compare(node->data, tmp->data)) {
                data = node->data;
                node->data = tmp->data;
                tmp->data = data;
            }
        }
    }

    return true;
}

bool llistAddAfter(Llist *l, int data, int after)
{
    if (!l || !l->head) {
        return false;
    }

    LlistNode *node = malloc(sizeof(*node));
    if (!node) {
        return false;
    }
    node->data = data;
    node->next = NULL;

    LlistNode *tmp = llistFind(l, after);
    if (!tmp) {
        free(node);
        return false;
    }

    node->next = tmp->next;
    tmp->next = node;

    return true;
}

bool llistDestroy(Llist *list)
{
    bool retVal = false;
    if (!list )
    {
        retVal = true;
        goto END;
    }
    else if (!list->head)
    {
        free(list);
        retVal = true;
        goto END;
    }

    LlistNode *tempNode = list->head;

    while(tempNode) {
        LlistNode *nextTemp = tempNode->next;
        free(tempNode);

        tempNode = nextTemp;
    }

    free(list);
    retVal = true;
END:
    return retVal;
}    


