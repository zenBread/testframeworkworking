
#include "../Headers/Cllist.h"


// Compare function for sorting
int cllistCompare(int a, int b)
{
    return a > b;
}


// Circularly Linked List Methods to implement

Cllist *
cllistNew(void)
{
    Cllist *retClist = calloc(1, sizeof(*retClist));
    if (!retClist) {
        return NULL;
    }

    return retClist;
}

bool 
cllistAdd(Cllist *list, int data)
{
    if (!list)
        return false;

    CllistNode *node = (CllistNode *)calloc(1, sizeof(CllistNode));
    if (!node)
        return false;
    node->data = data;

    if (!list->head) {
        // new head
        list->head = node;
        // new tail
        list->tail = node;
        // makes it circular
        node->next = node;
        goto Done;
    }

    // add to end to keep O(1) 
    // time for insertion
    list->tail->next = node;
    node->next = list->head;
    list->tail = node;

Done:
    return true;
}

CllistNode *
cllistFind(Cllist *list, int data)
{
    if (!list || !list->head)
        goto Done;

    CllistNode *curNode = list->head;
    CllistNode *lastNode = list->tail;

    while(curNode) {
        if (curNode->data == data) {
            return curNode;
        } else if (curNode == lastNode) {
            break;
        } else {
            curNode = curNode->next;
        }
    }

Done:
    return NULL;
}

CllistNode *
cllistRmHead(Cllist *list)
{
    if (!list || !list->head)
        return NULL;

    CllistNode *oldHead = list->head;
    CllistNode *newHead = NULL;
    if (oldHead->next == oldHead) {
        list->tail = NULL;
        list->head = NULL;
        return oldHead;
    }

    if (oldHead->next) {
        newHead = oldHead->next;
    }

    list->head = newHead;
    list->tail->next = newHead;

    return oldHead;
}

CllistNode *
cllistRmTail(Cllist *list)
{
    if (!list || !list->head || !list->tail)
        return NULL;

    CllistNode *curNode = list->head;

    CllistNode *oldTail = list->tail;
    CllistNode *newTail = NULL;

    if (curNode->next == curNode) {
        list->tail = NULL;
        list->head = NULL;
        return curNode;
    }

    while (curNode) {
        if (curNode->next == oldTail)
            break;
        curNode = curNode->next;
    }

    newTail = curNode;
    newTail->next = list->head;
    list->tail = newTail;

    return oldTail;
}

CllistNode *
cllistRm(Cllist *list, int data)
{
    if (!list || !list->head)
        return NULL;

    //Check the head
    if (list->head->data == data) {
        return cllistRmHead(list);
    }

    CllistNode *tempNode = NULL;
    CllistNode *prevNode = list->head;
    CllistNode *curNode = prevNode->next;

    while(curNode != list->head) {
        if (curNode->data == data) {
            // is the found data the tail?
            if (curNode == list->tail) {
                return cllistRmTail(list);
            }
            tempNode = curNode;
            prevNode->next = curNode->next;
            break;
        }

        prevNode = curNode;
        curNode = curNode->next;
    }

    return tempNode;
}

bool 
cllistDestroy(Cllist *list)
{
    if (!list)
        return false;

    if (!list->head) {
        free(list);
        return true;
    }

    CllistNode *tempNode = list->head;
    if (tempNode->next == tempNode) {
        free(tempNode);
    } else {
        while (tempNode) {

            CllistNode *nextTemp = tempNode->next;
            free(tempNode);
            if (nextTemp == list->tail) {
                free(nextTemp);
                break;
            }
            tempNode = nextTemp;
        }
    }
    free(list);
    return true;
}

bool 
cllistInsertAfter(Cllist *list, int data, int after)
{
    if (!list || !list->head)
        return false;

    CllistNode *node = (CllistNode *)calloc(1, sizeof(CllistNode));
    if (!node)
        return false;
    node->data = data;

    // Insert after tail
    if (list->tail->data == after) {
        node->next = list->tail->next;
        list->tail->next = node;
        list->tail = node;
        return true;
    }

    CllistNode *curNode = list->head;
    while (curNode != list->tail) {
        if (after == curNode->data) {
            node->next = curNode->next;
            curNode->next = node;
            break;
        }
    }
    return true;
}

bool cllistSort(Cllist *l, int (*Compare)(int, int))
{
    if (!l || !l->head) {
        return false;
    }

    CllistNode *node, *temp;
    int tmpData;
    for (node = l->head; node->next != l->head; node = node->next) {
        for (temp = node->next; temp != l->head; temp = temp->next) {
            if (Compare(node->data, temp->data)) {
                tmpData = node->data;
                node->data = temp->data;
                temp->data = tmpData;
            }
        }
    }
    return true;
}
