FROM ubuntu:latest

ADD . /test

RUN apt-get update -y && apt-get install libcunit1-dev make gcc -y

WORKDIR /test
RUN make
CMD [ "./BIN/CheckOnLearning", "SolutionObjects/" ]
