# __TDQC Phase 2 Check On Learning Practical__
## Data Structures

This project is the Source code for the Phase 2 Check On Learning for TDQC

Create, add, manipulate, and remove will be tested for:
    * Linked Lists
    * Doubly Linked Lists
    * Circularly Linked Lists
    * Binary Trees
    * Graphs

_This assessment is to accompany the written examination. _
