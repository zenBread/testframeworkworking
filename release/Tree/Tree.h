#ifndef TREE_H
#define TREE_H

#include <stdbool.h>

typedef struct _TreeNode TreeNode;

struct _TreeNode {
    int data;
    TreeNode *left;   
    TreeNode *right;   
};

typedef struct _tree {
    TreeNode *head;
} Tree, *Ptree;

// Binary Tree Methods to implement

/* treeNew() creates a new tree
    Args:
        No arguments
    Return:
        Ptree (also equal to tree*) to a newly allocated tree.
*/
Ptree treeNew(void);

/* treeAdd() adds to existing tree
    Args:
        Ptree tr : the Ptree to add the element to
        int data : the specific data element to add
    Return:
        bool: true on success, false on failure
*/
bool treeAdd(Ptree tr, int data);

/* treeFind() searches a tree for the first occurance of a given value
    Args:
        Ptree tr : the Ptree to find the element in
        int data : the specific data element to find
    Return:
        TreeNode* of the found node. null if node not found.
*/
TreeNode *treeFind(Ptree tr, int data);

/* treeRmHead() removes only the head of a tree
    Args:
        Ptree tr : the Ptree to remove the head from
    Return:
        TreeNode* of the removed head. null if tr is empty.
*/
TreeNode *treeRmHead(Ptree tr);

/* treeDestroy() destroys a tree
    Args:
        Ptree tr : Ptree  to destroy
    Return:
        bool : true on success, false of failure
*/
bool treeDestroy(Ptree tr);

#endif
