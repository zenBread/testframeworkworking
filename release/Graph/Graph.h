#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>

typedef struct _Edge Edge;
typedef struct _Node Node;
typedef struct _GraphNode GraphNode;

struct _Edge {
    int weight;
    Node *destNode;
    Edge *edgeNode;
};

struct _Node {
    int data;
    Node *next;
    Edge *edge;
};

typedef struct _GraphNode {
    Node *node;
    GraphNode *next;
} GraphNode;

typedef struct _Graph {
    GraphNode *nodes;
} Graph, *Pgraph;

// Graph Methods to implement

/* graphNew() creates a new graph
    Args:
        No arguments
    Return:
        Pgraph (also equal to graph*) to a newly allocated graph.
*/
Pgraph
graphNew(void);

/* graphAddNode() adds node to existing graph
    Args:
        Pgraph g : the pgraph to add the element to
        int data : the specific data element to add
    Return:
        bool: true on success, false on failure
*/
bool 
graphAddNode(Pgraph g, int data);

/* graphAddEdge() add edge to existing graph
    Args:
        Pgraph g  : the pgraph to add the element to
        struct src: the source node to add an edge to
        struct dst: the destination node to add edge to
    Return:
        bool: true on success, false on failure
*/
bool 
graphAddEdge(Pgraph g, Node *src, Node *dst);

/* graphFind() searches a graph for the first occurance of a given value
    Args:
        Pgraph g : the pgraph to find the element in
        int data : the specific data element to find
    Return:
        Node* of the found node. null if node not found.
*/
Node *
graphFind(Pgraph g, int data);

/* graphRmNode() removes the node and its edges from a graph
    Args:
        Pgraph g : the Pgraph to remove the head from
        int data : the specific data to remove
    Return:
        bool: true on success, false on failure
*/
bool
graphRmNode(Pgraph g, int data);

/* graphRmEdge() removes the edge from a graph
    Args:
        Pgraph g : the Pgraph to remove the head from
        struct src: the source node to add an edge to
        struct dst: the destination node to add edge to
    Return:
        bool: true on success, false on failure
*/
bool
graphRmEdge(Pgraph g, Node *src, Node *dst);

/* graphDestroy() destroys a graph
    Args:
        Pgraph g : Pgraph  to destroy
    Return:
        bool : true on success, false of failure
*/
bool 
graphDestroy(Pgraph g);

#endif
