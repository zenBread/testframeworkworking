#ifndef GRAPH_H
#define GRAPH_H

#include <stdbool.h>

typedef struct _Edge Edge;
typedef struct _Node Node;

struct _Edge {
    int weight;
    Node *destNode;
    Edge *edgeNode;
};

struct _Node {
    int data;
    Node *next;
    Edge *edge;
};

typedef struct _AdjacencyList {
    Node *nodes;
} Graph, *Pgraph;

// Graph Methods to implement

/* graphNew() creates a new graph
    Args:
        No arguments
    Return:
        Pgraph (also equal to graph*) to a newly allocated graph.
*/
Pgraph
graphNew(void);

/* graphAddNode() adds node to existing graph
    Args:
        Pgraph pGraph : the pgraph to add the element to
        int data : the specific data element to add
    Return:
        bool: true on success, false on failure
*/
bool 
graphAddNode(Pgraph pGraph, int data);

/* graphAddEdge() add edge to existing graph
    Args:
        Pgraph pGraph  : the pgraph to add the element to
        struct srcNode: the source node to add an edge to
        struct dstNode: the destination node to add edge to
    Return:
        bool: true on success, false on failure
*/
bool 
graphAddEdge(Pgraph pGraph, Node *srcNode, Node *dstNode);

/* graphFind() searches a graph for the first occurance of a given value
    Args:
        Pgraph pGraph : the pgraph to find the element in
        int data : the specific data element to find
    Return:
        Node* of the found node. null if node not found.
*/
Node *
graphFind(Pgraph pGraph, int data);

/* graphRmNode() removes the node and its edges from a graph
    Args:
        Pgraph pGraph : the Pgraph to remove the head from
        int data : the specific data to remove
    Return:
        bool: true on success, false on failure
*/
bool
graphRmNode(Pgraph pGraph, int data);

/* graphRmEdge() removes the edge from a graph
    Args:
        Pgraph pGraph : the Pgraph to remove the head from
        struct srcNode: the source node to add an edge to
        struct dstNode: the destination node to add edge to
    Return:
        bool: true on success, false on failure
*/
bool
graphRmEdge(Pgraph pGraph, Node *srcNode, Node *dstNode);

/* graphDestroy() destroys a graph
    Args:
        Pgraph pGraph : Pgraph  to destroy
    Return:
        bool : true on success, false of failure
*/
bool 
graphDestroy(Pgraph pGraph);

#endif
