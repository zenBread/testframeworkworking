#include "Tests/LlistTests.h"
#include "Tests/DllistTests.h"
#include "Tests/CllistTests.h"
#include "Tests/TreeTests.h"
#include "Tests/GraphTests.h"
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>

#define PROJECT_TITLE "\n\tTool Developer Qualification Course \n\tPhase 2 - Data Structures Check on Learning (COL)\n"

enum {
    COL_SUCCESS = 0,
    COL_GENERAL_ERROR,
    COL_INVALID_ARGUMENT,
    COL_SUITE_ERROR,
    COL_HELP
};

typedef struct _options
{
    bool Llist;
    bool Dlist;
    bool Clist;
    bool Tree;
    bool Graph;
    bool help;
    bool error;
} Options;


extern int optind;

/*
 * Phase 2 Data Structure Check on learning.
 * Unit tests to validate proper functionality of user 
 * implemented data structures for:
 *     - Linked Lists
 *     - Doubly Linked Lists
 *     - Circularly Linked Lists
 *     - Trees
 *     - Graphs
*/



static int parseArgs(int argc, char **argv, Options *opts);
static void printHelp(char *programName);

int main(int argc, char *argv[])
{
    int retVal = COL_GENERAL_ERROR;
    char *programName = argv[0];
    Options options = { 
        .Llist = true,
        .Dlist = true,
        .Clist = true,
        .Tree = true,
        .Graph = true,
        .help = false,
        .error = false,
    };
    int optInd = parseArgs(argc, argv, &options);
    argc -= optInd;
    argv += optInd;

    if (options.help)
    {
        printHelp(programName);
        retVal = COL_HELP;
        goto END;
    }
    else if(options.error)
    {
        printHelp(programName);
        retVal = COL_INVALID_ARGUMENT;
        goto END;
    }

    srand(time(NULL));
    char *uName = getlogin();

    if (options.Llist)
    {
        /* ------ Llist test suites ------ */
        printf("%s%s\nUsername: %s", PROJECT_TITLE, "\t- Linked List Tests\n", uName);
        if (mainLlistSuite(*argv))
        {
            retVal = COL_SUITE_ERROR;
            goto END;
        }
    }
    
    if (options.Dlist)
    {
        /* ------ Dllist test suites ------ */
        printf("%s%s\nUsername: %s", PROJECT_TITLE, "\t- Doubly Linked List Tests\n", uName);
        if (mainDllistSuite(*argv))
        {
            retVal = COL_SUITE_ERROR;
            goto END;
        }
    }

    if (options.Clist)
    {
        /* ------ Cllist test suites ------ */
        printf("%s%s\nUsername: %s", PROJECT_TITLE, "\t- Circularly Linked List Tests\n", uName);
        if (mainCllistSuite(*argv))
        {
            retVal = COL_SUITE_ERROR;
            goto END;
        }
    }
    if (options.Tree)
    {
        /* ------ Tree test suites ------ */
        printf("%s%s\nUsername: %s", PROJECT_TITLE, "\t- Binary Tree Tests\n", uName);
        if (mainTreeSuite(*argv))
        {
            retVal = COL_SUITE_ERROR;
            goto END;
        }
    }
    if (options.Graph)
    {
        /* ------ Graph test suites ------ */
        printf("%s%s\nUsername: %s", PROJECT_TITLE, "\t- Graph Tests\n", uName);
        if (mainGraphSuite(*argv))
        {
            retVal = COL_SUITE_ERROR;
            goto END;
        }
    }
    retVal = COL_SUCCESS;
END:
    return retVal;
}

static int parseArgs(int argc, char **argv, Options *opts)
{
    int opt;
    bool lflag, dflag, cflag, tflag, gflag, aflag;
    lflag = dflag = cflag = tflag = gflag = aflag = false;

    while ((opt = getopt(argc, argv, "hldctga")) != -1)
    { 
        switch(opt)
        {
        case 'h':
            opts->help = true;
            return optind;
        case 'l':
            lflag = true;
            break;
        case 'd':
            dflag = true;
            break;
        case 'c':
            cflag = true;
            break;
        case 't':
            tflag = true;
            break;
        case 'g':
            gflag = true;
            break;
        case 'a':
            aflag = true;
            break;
        case '?':
            opts->error = true;
            fprintf(stderr, "Invalid argument: '%c'\n", opt);
            return optind;
        }
    }
    if (!aflag && optind > 1)
    {
        opts->Clist = cflag;
        opts->Dlist = dflag;
        opts->Llist = lflag;
        opts->Graph = gflag;
        opts->Tree  = tflag;
    }
    if (optind + 1 > argc)
    {
        fprintf(stderr, "%s: Missing folder path.\n", argv[0]);
        opts->error = true;
    }
    return optind;
}

static void printHelp(char *programName)
{
    printf("Syntax: %s [- l | d | c | t | g | a ] <folder>\n", programName);
    printf("\t-l:\tLinked Lists\n"
           "\t-d:\tDoubly Linked Lists\n"
           "\t-c:\tCircullarly Linked Lists\n"
           "\t-t:\tTrees\n"
           "\t-g:\tGraphs\n"
           "\t-a:\tAll tests\n");
}
